
<!DOCTYPE html>
<html lang="en">

  <head>

  
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-85902563-8"></script>
<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-MFMSCQD':true});</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-85902563-8');
  ga('require', 'GTM-MFMSCQD');
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-85902563-8', 'auto');
    ga('require', 'GTM-MFMSCQD');
    ga('send', 'pageview');
  </script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

    <title>#Formation Le Grand HACK</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"> </script>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lobster+Two' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Berkshire+Swash' rel='stylesheet' type='text/css'>

 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="css/new-age.min.css" rel="stylesheet">


<!-- VOIR A RETIRER OU NON SECOND CSS  -->
      <!-- Custom fonts for this template -->
   
    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
    <script src="js/flipclock.min.js"></script>
    <link rel="stylesheet" href="css/flipclock.css">

<!-- GOOD FOR THE COUNTDOWN -->

    <link rel="stylesheet" href="css/flipclock.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script src="js/flipclock.js"></script>

  <script>
  
  </script>





  
<!-- END GOOD FOR THE COUNTDOWN -->

    <!-- END VOIR A RETIRER OU NON SECOND CSS  -->
<style type="text/css">

@media screen and (min-width: 991px)  {
  .espererBloc{
    margin-top:-390px !important;
  }
  .questionQuatre{
      margin-top: -95px !important;
  }
  .question2{
    margin-top: 25% !important;
  }
  #price{
    margin-top: -10%;
  }
}

.pricing-header {
  max-width: 700px;
}

.card-deck .card {
  min-width: 220px;
}

.counter {
    background-color:#f5f5f5;
    padding: 20px 0;
    border-radius: 5px;
}

.count-title {
    font-size: 40px;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 0;
    text-align: center;
}

.count-text {
    font-size: 13px;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 0;
    text-align: center;
}

.fa-2x {
    margin: 0 auto;
    float: none;
    display: table;
    color: #4ad1e5;
}
 
 .strikeout {
  font-size: 20px;
  color: red;
  line-height: 1em;
  position: relative;
}
.strikeout::after {
  border-bottom: 0.120em solid red;
  content: "";
  left: 0;
  margin-top: calc(0.175em / 2 * -1);
  position: absolute;
  right: 0;
  top: 50%;
}
.promotion{
  font-size: 12px;
  font-family: lato;
}
span.noelHover:hover {
    font-family: Pacifico;
}
h1,h2,h3,h4,h5,h6,p,a {
    color: black;
}

</style>
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">#Formation Le Grand HACK</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
          <!--  <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#download">La Formation</a>
            </li>  -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#features"  >9 Jours de Grow-Hacking</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact" >Question/Réponse</a>
            </li>
             <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#price" >Let's Start !</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-12 my-auto">
            <div class="header-content mx-auto">
              <h1 style="background-color: rgba(0, 0, 0, 0.5);;text-align: center; max-width: 800px;font-size: 60px; font-weight: 600; width: 90%;margin-right: auto;margin-left: auto; color: white !important"> Formation Dropshipping -</h1>
              <h1 style="background-color: rgba(0, 0, 0, 0.5); width: 60%;margin-right: auto;margin-left: auto;text-align: center; max-width: 800px;font-size: 68px; font-weight: 700; margin-bottom: 50px; color: white !important;"> Le grand #HACK </h1>
              <h1 class="mb-5"  style="color:black;background-color: #fcc64d;    width: 60%;margin-right: auto;margin-left: auto;border: white 6px solid;    border-radius: 5px; text-align:center; margin-bottom: 50px">9 Jours de GrowHacking </br> = 9 Jours de Résultats</h1>
              <h1 class="mb-5"  style="width: 60%; color:white !important; margin-right: auto;margin-left: auto; text-align:center; font-size: 80px;">&#x2193</h1>
              <div class="mb-5"  style="  width: 60%;margin-right: auto;margin-left: auto; text-align:center;">
              <a href="#features" class="btn btn-outline btn-xl js-scroll-trigger" style="text-align: center; color: white !important;">9 J de GrowHacking-Voir le détail</a>
              </div>
            </div>
          </div>
         
                 <!-- <div class="button">     
                   You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    
 <section class="laFormation"  style="background-color: #fcc64d;" id="features">
      <div class="container">
        <div class="section-heading text-center">
          <h2 style="margin-bottom: 40px;  ">9 jours de Growhacking = 9 jours de résultats</u> </h2>
           <p style="padding-right: 150px;padding-left: 150px; margin-bottom: 20px;" >Découvre une formation structurée en 9 jours pour appliquer des techniques de Grow-Hacking dans ta boutique en dropshipping et avoir des résultats réels! </p>

          <p class="text-muted">Pré-requis : 9 jours de motivation pour mettre tout ça en place !</p>
          <hr>
        </div>
        <div class="row">
          <div class="col-lg-12 my-auto" style="margin-left: 20%;">
            <img src="img/formation-1.png" style="max-width: 60%;box-shadow: 14px 11px 0px 3px #00000036;
    border: 1px black solid;">
       
 
              </div>
            </div>
          </div>
         
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>



 <!-- About -->
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center" style="padding-bottom: 40px;">
            <h2 class="section-heading text-uppercase">Start</h2>
            <h3 class="section-subheading text-muted" >Le détail de la formation</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <ul class="timeline">
                <!-- GAUCHE -->
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle1.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-1</h4>
                    <h4 class="subheading" style="font-weight: 500;">#Hack shopify</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted" style="    margin-left: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">Shopify à un tas de fonctionalités et de plug-in top et c’est dûr de s’y retrouver. Apprends à utiliser le meilleur de shopify durant cette 1er journées avec la mise en place de différents Hacks.
  </br> On verra aussi les SEULS plug-in GRATUITS dont tu as besoin pour avoir des résultats. 
</br>
A la fin de cette journée tu auras mit en place ta boutique shopify avec un look PRO.  </p>
                  </div>
                </div>
              </li>
  <!--END GAUCHE -->
              <!-- DROITE -->
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle2.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-2</h4>
                    <h4 class="subheading"  style="font-weight: 500;">#Hack le meilleur produit/niche</h4>
                  </div>
                  <div class="timeline-body">
                   <p class="text-muted" style="    margin-right: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">Ici je te montrerai les meilleurs outils (gratuits) à utiliser pour trouver les meilleurs produits. </br>On verra comment mettre les points essentiels à prendre en compte pour être sûr que sa niche soit bien choisie et <b>ne pas perdre de temps et d'argent avec une mauvaise niche.</b>
 
                  </div>
                </div>
              </li>

                  <!--END DROITE -->
         <!-- GAUCHE -->
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle3.png" alt="">
                </div>
                    <div class="timeline-image" style="margin-top: 300px;">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle4-1.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-3</h4>
                    <h4 class="subheading" style="font-weight: 500;"># Hack la page de vente qui convertit au top</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted" style="    margin-left: -50px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">
Tu as peut être déjà des ventes ou non, cela n’a pas d’importance, on va ici Hacker tes pages de ventes et optimiser au Max le traffic, les clics, et les ventes ! 
</br>
Quel type de texte pour convaincre ton audience de cliquer sur le bouton Acheter ? Quel couleur de fond ?(etc )
Ce sont plein d’élements qui doivent être optimisés ! 
</br>
</br>
Tu vas comprendre qu’il y’a toujours un grand écart entre ce que ta page de vente te rapporte et ce qu’elle pourrait te rapporter apres optimisation. 
</br>
</br>
L’optimisation implique des statistiques de pages et autre, et au départ ça peut faire peur ! Pas de craintes ! 
</br>
Ici je vais te montrer les chiffres dont tu as vraiment besoin de prendre en compte pour optimiser TA page de vente et l'optimiser de telle maniere à avoir la MEILLEURE version de ta page de vente. 
</br>
Pour ça on va utiliser des outils gratuits et faciles à mettre en place. 
      </p>
                  </div>

                </div>
              </li>
  <!--END GAUCHE -->
             <!-- DROITE -->
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle5.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-4</h4>
                    <h4 class="subheading"  style="font-weight: 500;">#Hack le traffic</h4>
                  </div>
                  <div class="timeline-body">
                   <p class="text-muted" style="    margin-right: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">Hack le traffic gratuit ! Tu peux tout hacker avec un peu de pratique, que ce soit la Seo (le référencement sur google par exemple), le traffic provenant des réseaux sociaux. 
</br>
On va ici appliquer des techniques de growhacking pour multiplier ton traffic gratuit. 
</br>
Tu vas aussi apprendre à trouver le bon traffic. C’est cool d’avoir des visiteurs sur sa boutique mais c’est encore bien mieux d’avoir parmi ses visiteurs des ACHETEURS. Tout traffic n’est pas forcément bon à prendre. Tu vas ici comprendre comment avoir le BON type de traffic pour TA niche ! 
 
                  </div>
                </div>
              </li>

                  <!--END DROITE -->
  <!-- GAUCHE -->
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle6.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-5</h4>
                    <h4 class="subheading" style="font-weight: 500;">#Hack les réseaux sociaux</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted" style="    margin-left: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">Les réseaux sociaux lorsqu'ils sont bien utilisés peuvent être une mine d'or pour ton business ! Aujourd'hui des tas de techniques de grow-hacking très peu exploitées permettent de prendre avantage des réseaux sociaux. 
      </br> 
    On verra entre autre comment acquérir une premiere liste de prospect hyper ciblée (e-mail, data facebook) sans aucun budget! </p>
                  </div>
                </div>
              </li>
  <!--END GAUCHE -->

  <!-- DROITE -->
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle7.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-6</h4>
                    <h4 class="subheading"  style="font-weight: 500;">#Hack tes compétiteurs </h4>
                  </div>
                  <div class="timeline-body">
                   <p class="text-muted" style="    margin-right: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">Pourquoi Hacker ses competiteurs ? (en toute légalité…) 
      </br>
      Tout simplement parce que tes compétiteurs ont déja fait 60 % du boulot et <b> 90% des erreurs </b> que tu ne veux pas faire ! 
    </br>
    Hacker ses competiteurs c’est pouvoir gagner du TEMPS et éviter les dépenses inutiles ! 
 
                  </div>
                </div>
              </li>

                  <!--END DROITE -->

<!-- GAUCHE -->
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle8.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-7</h4>
                    <h4 class="subheading" style="font-weight: 500;">#Hack des mails</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted" style="    margin-left: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">Perso je fais 35% de mes ventes grâce à ma liste de mails, autant dire qu’il s’agit là d’une part à pas négliger. 
      </br>
      Avec 1000-2000 il est possible de parvenir à générer un bon premier chiffre d’affaire.  
</br>
On peut avoir 1000 mails en un mois avec 33 mails par jours, 33x 30 jours = 990 exactement !
</br> Autant dire que ce n’est pas hyper difficile à acquérir avec des bon hacks mit en place.
</br> On verra comment acquérir les mails d’acheteurs potentiels de sa niche!  </p>
                  </div>
                </div>
              </li>
  <!--END GAUCHE -->

  <!-- DROITE -->
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle9.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-8</h4>
                    <h4 class="subheading"  style="font-weight: 500;">#Hack la data Facebook </h4>
                  </div>
                  <div class="timeline-body">
                   <p class="text-muted" style="    margin-right: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">
        La différence entre un business à succès avec les pubs Facebook et celui qui gaspille tout son budget sans résultats sur facebook n’est pas uniquement une question de ciblage, mais bien une question d’avoir assez de données, de ‘'DATA'' Facebook. 
</br>
Plus tu as de « Data » et plus Facebook va pouvoir montrer tes publicités à des acheteurs potentiels. 
</br>
Pour cela il va falloir acquerir de la Data Facebook et lorsqu'on débute ça peut sembler assez déroutant. On va ici mettre en place des techniques pour acquérir une première couche de data suffisante pour lancer ses premieres pubs avec un ciblage adéquat. 
 
                  </div>
                </div>
              </li>

                  <!--END DROITE -->
                  <!-- GAUCHE -->
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle10.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">JOUR-9</h4>
                    <h4 class="subheading" style="font-weight: 500;">#Hack les partenaires</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted" style="    margin-left: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">
        Un business en e-commerce devient populaire non pas parce qu’il a une page Facebook de 10 000 fans mais bien parce qu’il à pu acquérir un tas de partenaires qui permettent ce business de prospérer ! 
</br>
Il s’agit la d’un point que bien 90% des dropshippers ont tendances à oublier de mettre en place. 
</br>
</br>
Un partenariat en business est un échange de valeurs. Lorsqu’on débute on peut penser que la seule possibilité d’acquérir des partenaires et de les payer ! 
</br>
Et pourtant non…. il y’a des tas de moyens d’acquerir de nouveaux partenaires qui vont faire décoller ta boutiques grâce à leur influence en quelques semaines… 
</br>
Il suffit pour cela de déterminer les valeurs qu’on peut ‘échanger’ et de mettre des techniques en place pour acquérir ses nouveaux partenaires !  </p>
                  </div>
                </div>
              </li>
  <!--END GAUCHE -->
 <!-- DROITE -->
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/cercle13.png" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 style="font-weight: 500;">VIP</h4>
                    <h4 class="subheading"  style="font-weight: 500;">#Hack le groupe Facebook </h4>
                  </div>
                  <div class="timeline-body">
                   <p class="text-muted" style="    margin-right: -40px;
        font-family: Catamaran,Helvetica,Arial,sans-serif;text-align: justify;font-size: 16px;text-align: justify;">
     Accède au groupe facebook de Growhackers réservé à la formation. Growhack durant ces 9 jours, partage tes résultats et découvre les résultats des autres membres !
 
                  </div>
                </div>
              </li>

                  <!--END DROITE -->



              <li class="timeline-inverted">
                <div class="timeline-image">
                  <h4>Let's
                    <br> <span style="font-size: 25px;font-weight: 300;">Grow-Hack 
                    <br> <span style="font-size: 20px;">!</span></h4>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>



    <section class="download bg-primary text-center" id="download">
      <div class="container">
        <div class="row" style="margin-top: -120px; margin-bottom: -90px;">
          <div class="col-md-8 mx-auto">
            <h2 class="section-heading"> <span style="color:black;font-size: 100px; font-weight:100;">#</span> </br>   FORMATION LE GRAND HACK </h2>
            <p style="font-size:23px; padding-bottom: 20px;"> <span style="font-weight:200; color: black;">#</span>  <b>Pas de perte de temps :</b>  Formation sans blabla inutile </br> <span style="font-size: 25px;"> > </span> 9 jours de formations = 9 jours de <u>résultats</u></p>
                 <p style="font-size:23px; color: black;"> <span style="font-weight:200;">#</span>  <b>Meilleur que les autre formations :</b>  </br> <span style="font-size: 25px;">  </span>Techniques de Growhacking misent a jour, uniques et originals !  </p>
            <i class="material-icons" style="padding-top: 30px; font-size: 60px;">flash_on</i>
          </div>
        </div>
      </div>
    </section>

<!-- question reponse 1  -->
    <section class="features" id="contact">
      <div class="container">
        <div class="section-heading text-center" style="margin-bottom: 10px !important;">
          <h2>Questions / Réponse </h2>
          <hr>
        </div>
     
          <div class="col-lg-12 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item" style="margin-left:1%">
                  
                    <i class="large material-icons" style="    width: 140%;">access_time</i>
                    <h3 style="    width: 140%;" >Est ce que je peux accéder à la formation plus de 9 jours ?</h3>
                    <p class="text-muted" style="    width: 140%;" >La formation à été conçue pour être mise en place en 9 jours mais tu peux bien entendu accéder à celle-ci à vie !</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item" style="margin-left:1%">
                    <i class="large material-icons question2" style="    width: 140%;" >insert_emoticon</i>


                    <h3 style="    width: 140%;" >Est ce que je peux facilement suivre cette formation si je n’ai pas de connaissance en programming/GrowHacking  ?</h3>
                    <p class="text-muted" style="width: 140%;" >Alors je vais en effet te montrer des hacks qui impliquent de copier/coller des scripts que tu pourras trouver au sein de l’espace de la formation. Tu auras pour ce type de hack juste à copier/coller mes scripts et suivre les videos en question, rien de compliqué en soi. 
</br>
 La formation se compose aussi d’autre techniques qui n’impliquent pas de programming. Cette formation ne demande donc pas d’avoir de quelquconque connaissance en programming. </p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item espererBloc" style="margin-left:1%">
                    <i class="large material-icons" style="width: 140%;" >attach_money</i>
                    <h3 style="width: 140%;" >Que puis-je espérer en terme de resultats ?</h3>
                    <p class="text-muted" style="width: 140%;" >Tu vas mettre en place des technqiues de Grow-hacking que j'utilise moi même tous les jours depuis maintenant deux ans,, Autant dire que tu pourras avoir des résultats aussi bien en terme de traffics, de partenariats, de nouveaux mails et de ventes dans les deux semaines qui suivent !</p>
                  </div>
                </div>
                <div class="col-lg-12" >
                  <div class="feature-item questionQuatre" style="margin-left:20%; margin-right:auto; width:50%;">
                    <i class="icon-lock-open text-primary" style="width: 180%;" ></i>
                    <h3 style="width: 180%;" >Quand vais je recevoir la formation apres mon inscription/ et l'accès au groupe privée facebook ? </h3>
                    <p class="text-muted" style="width: 180%;" >Tu recevras un e-mail pour accéder a la platerforme de la formation immédiatement après ton inscription ainsi qu'un lien pour accéder au groupe facebook privée. </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--   <div class="container">
        <div class="section-heading text-center" style="    margin-left: -4%;">
        <img src="https://fairedudropshipping.com/wp-content/uploads/2018/12/666.png">
          <hr>
        </div>   -->

    </section>
<!-- END question reponse  -->



<!-- PRICE   -->
      <section class="contact bg-primary" id="price">
          <p style="margin-bottom: -20px; margin-top: -50px; font-family: lato; font-size: 39px;">#Let's START !</p>
       <i class="material-icons" style="padding-bottom: 20px; font-size: 100px;">arrow_drop_down</i>
 <div class="container">

      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 id="price2" class="my-0 font-weight-normal" style="font-family: lato; font-size: 22px;">Formation #Le Grand HACK</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title" style="padding-bottom: 3%;"> € 115 <span class="strikeout">175<span style="font-size:17px;">€</span></span><small class="text-muted"></small></br><p class="promotion">115€ jusqu'au 12 décembre</p> </h1>

            <ul class="list-unstyled mt-3 mb-4">
              <li> Formation le Grand Hack</li>
              <li style="padding-bottom: 10%;">Groupe privé Facebook</li>
            </ul>
            <button id="customButton" type="button" class="btn btn-lg btn-block btn-primary" >Let's Hack !</button>
            <script src="https://checkout.stripe.com/checkout.js"></script>



<script>
var handler = StripeCheckout.configure({
  key: 'pk_test_JpsTcmNjo5MDqvI8AcPNLCNg',
 
  image : 'https://fairedudropshipping.herokuapp.com/img/1111.png',
  locale: 'auto',
  token: function(token) {
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
  }
});

document.getElementById('customButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler.open({
    name: 'Fairedudropshipping',
    description: 'Formation #Le Grand Hack',
    zipCode: true,
    currency: 'eur',
    amount: 11500
  });
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
</script>
<!--
<form action="submit.php" method="POST">
    <script
      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
      data-key="pk_test_JpsTcmNjo5MDqvI8AcPNLCNg"
      data-amount="999"
      data-name="Fairedudropshipping"
      data-description="Widget"
      data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
      data-locale="auto"
      data-zip-code="true"
      data-currency="eur">
    </script>
  </form>
  -->

<p>2</p>
<form action="/submit.php" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_JpsTcmNjo5MDqvI8AcPNLCNg"
    data-amount="999"
    data-name="fairedudropshipping.com
    data-description="Donation"
    data-image="http://www.ilovephp.net/wp-content/uploads/2016/03/small-3.jpg"
    data-locale="auto"
    data-zip-code="false">
  </script>
</form>





          </div>
        </div>

        <div class="card mb-4 shadow-sm">

          <div class="card-header">
                  <div id="div1" style="display:none; color: red;"><a href="#hackNoel" > <u>En savoir Plus</u></a></div>
            <h4 class="my-0 font-weight-normal savoirPlus" onmouseover="document.getElementById('div1').style.display = 'block';"   > +  Module<span class="noelHover"> #Hack Noël</span></h4>


          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">€150 <span class="strikeout">250<span style="font-size:17px;">€</span></span> <small class="text-muted"></small> </br><p class="promotion">150€ jusqu'au 12 décembre</p>    </h1>
            <ul class="list-unstyled mt-3 mb-4">
                <li>Module #Hack Noël</li>
              <li>Formation le Grand Hack</li>
              <li style="padding-bottom: 9%;">Groupe privé Facebook</li>
            
            </ul>
            <button id="customButton2" type="button" class="btn btn-lg btn-block btn-outline-primary">Let's Hack !</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">+ Coaching pendant 2 mois</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">€220 <span class="strikeout">360<span style="font-size:17px;">€</span></span><small class="text-muted"></small>  </br><p class="promotion">220€ jusqu'au 12 décembre</p>   </h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Coaching de 2 mois via Tchat</li>
                 <li>Module #Hack Noël</li>
              <li>Formation le Grand Hack</li>
              <li style="padding-bottom: 1%;">Groupe privé Facebook</li>
            </ul>
            <button id="customButton3" type="button" class="btn btn-lg btn-block btn-primary">Let's Hack !</button>
          </div>
        </div>
      </div>


      <script>
            var handler = StripeCheckout.configure({
              key: 'pk_test_JpsTcmNjo5MDqvI8AcPNLCNg',
              image: 'https://fairedudropshipping.herokuapp.com/img/1111.png',
              locale: 'auto',
              token: function(token) {
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.
              }
            });
            
            document.getElementById('customButton2').addEventListener('click', function(e) {
              // Open Checkout with further options:
              handler.open({
                name: 'Fairedudropshipping',
                description: 'Formation #Le Grand Hack',
                zipCode: true,
                currency: 'eur',
                amount: 15000
              });
              e.preventDefault();
            });
            
            // Close Checkout on page navigation:
            window.addEventListener('popstate', function() {
              handler.close();
            });
            </script>


<script>
        var handler = StripeCheckout.configure({
          key: 'pk_test_JpsTcmNjo5MDqvI8AcPNLCNg',
          image: 'https://fairedudropshipping.herokuapp.com/img/1111.png',
          locale: 'auto',
          token: function(token) {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use.
          }
        });
        
        document.getElementById('customButton3').addEventListener('click', function(e) {
          // Open Checkout with further options:
          handler.open({
            name: 'Fairedudropshipping',
            description: 'Formation #Le Grand Hack',
            zipCode: true,
            currency: 'eur',
            amount: 22000
          });
          e.preventDefault();
        });
        
        // Close Checkout on page navigation:
        window.addEventListener('popstate', function() {
          handler.close();
        });
        </script>




       <p style="font-family: lato; font-size: 39px;">Pour l'ouverture de la formation, Hack la promotion ! </p>
     <p style="margin-top:-7px; font-size:20px;">  €115 au lieu de <span class="strikeout">175<span style="font-size:17px;">€</span></span> pour la formation Le Grand Hack</p>
        <p style=" margin-top:-10px; padding-bottom:15px;font-family: lato; font-size: 34px;"> <u>Disponible jusqu'au 12 Décembre 23h : </u> </p>
     

<div class="clock" style="margin:2em; margin-left: auto; margin-right: auto; width: 60%;"></div>
  <div class="message" style="margin-bottom: -30px;"></div>

 <!--   <i class="material-icons" style="padding-bottom: 12px; font-size: 100px;">access_alarm</i>  -->

  <script type="text/javascript">
    var clock;
    
    $(document).ready(function() {
      var clock;

      clock = $('.clock').FlipClock({
            clockFace: 'DailyCounter',
            autoStart: false,
            callbacks: {
              stop: function() {
                $('.message').html('The clock has stopped!')
              }
            }
        });
            
        clock.setTime(620880);
        clock.setCountdown(true);
        clock.start();

    });
  </script>


<section  style="margin-bottom:-130px !important;" >
    <div id="countdown">
        <div id='tiles'></div>
        <div class="labels">
          <li>Jours</li>
          <li>Heures</li>
          <li>Mins</li>
          <li>Secs</li>
        </div>
      </div>
    </section>


    <hr style="border: 1px #636161 solid; margin-top: 60px;"">
  



<div class="container">
    <div class="row" ;>
      <div class="col-md-6 garanti-image" > <img src="img/GARANTI1.png" style="     " alt=""> </div>
          <div class="col-md-6 garanti" style="font-size:15px;color:black !important"  > GARANTIE DE REMBOURSEMENT </br> Garantie au résultat <a href="#popup1"><u>Plus d'infos</u>  </a></div>
  </div>
  

    
  <style>
  @media (min-width: 768px) {
  
.garanti-image{
  text-align: right;
}

.garanti{
  text-align:left; 
  padding-top: 90px;
}
 
.col-md-6.garanti-image img{
  padding-top: 50px;
   margin-left: 10%;
  width: 25%;
 }
  
}
@media (max-width: 767px) {
  
  .col-md-6.garanti-image img{
   width: 30% !important;
   padding-top:30px;
  }
  .garanti{
    padding-top: 20px;
    text-align: center;
  }
  
    
  }
  </style>



<!--   pop up  -->

<div id="popup1" class="overlay">
    <div class="popup" style="padding-left: 10px; padding-right:10px;padding:30px;">
      <h3 style="font-family:lato; padding-right: 20px; color:black !important; font-size: 25px; padding-top: 10px;"  >Garantie de remboursement aux résultats</h3>
      <a class="close" href="#price2">&times;</a>
      <div class="content" style="color:black !important; font-size: 15px;" >
        Je garantis un remboursement de la formation aux résultats valable 30 jours.</br> 
   Simplement en m'envoyant un e-mail me prouvant que tu n'as pas eu de résultats en appliquant mes méthodes.
      </div>
    </div>
  </div>


<style>

  div#popup1 {
    z-index: 4;
}
.box {
  width: 40%;
  margin: 0 auto;
  background: rgba(255,255,255,0.2);
  padding: 35px;
  border: 2px solid #fff;
  border-radius: 20px/50px;
  background-clip: padding-box;
  text-align: center;
}

.button {
  font-size: 1em;
  padding: 10px;
  color: #fff;
  border: 2px solid #06D85F;
  border-radius: 20px/50px;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.3s ease-out;
}
.button:hover {
  background: #06D85F;
}

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;

  width: 40%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}
.popup {
    background-color: #fcc54d;
}

@media screen and (max-width: 700px){
  .box{
    width: 70%;
  }
  .popup{
    width: 70%;
  }
}

</style>



 <!-- end pop up   -->





    <!-- END PRICE -->

<!-- COUNTER -->

   <!-- <section class="contact " id="trust"> -->

</section>

<!-- END COUNTER -->


<!-- NEW COUTNER-->


  <script>
  var target_date = new Date().getTime() + (8000*2020*48); // set the countdown date
var days, hours, minutes, seconds; // variables for time units

var countdown = document.getElementById("tiles"); // get tag element

getCountdown();

setInterval(function () { getCountdown(); }, 1000);

function getCountdown(){

	// find the amount of "seconds" between now and target
	var current_date = new Date().getTime();
	var seconds_left = (target_date - current_date) / 1000;

	days = pad( parseInt(seconds_left / 86400) );
	seconds_left = seconds_left % 86400;
		 
	hours = pad( parseInt(seconds_left / 3600) );
	seconds_left = seconds_left % 3600;
		  
	minutes = pad( parseInt(seconds_left / 60) );
	seconds = pad( parseInt( seconds_left % 60 ) );

	// format countdown string + set tag value
	countdown.innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>"; 
}

function pad(n) {
	return (n < 10 ? '0' : '') + n;
}


  </script>

  <style>
 
#countdown{
	width: 465px;
	height: 112px;
  text-align: center;
  
  margin-top: -30px !important;
  /*
	background: #222;
	background-image: -webkit-linear-gradient(top, #222, #333, #333, #222); 
	background-image:    -moz-linear-gradient(top, #222, #333, #333, #222);
	background-image:     -ms-linear-gradient(top, #222, #333, #333, #222);
	background-image:      -o-linear-gradient(top, #222, #333, #333, #222);
	border: 1px solid #111;
	border-radius: 5px;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.6);
  */
	margin: auto;
	padding: 24px 0;
/*	position: absolute; */
  top: 0; bottom: 0; left: 0; right: 0;
}

#countdown:before{
	content:"";
	width: 8px;
  height: 65px;
  
   /*
	background: #444;
	background-image: -webkit-linear-gradient(top, #555, #444, #444, #555); 
	background-image:    -moz-linear-gradient(top, #555, #444, #444, #555);
	background-image:     -ms-linear-gradient(top, #555, #444, #444, #555);
	background-image:      -o-linear-gradient(top, #555, #444, #444, #555);
	border: 1px solid #111;
	border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
  */
	display: block;
/*	position: absolute; */
	top: 48px; left: -10px;
}

#countdown:after{
	content:"";
  width: 8px;

  height: 65px;
   /*
	background: #444;
	background-image: -webkit-linear-gradient(top, #555, #444, #444, #555); 
	background-image:    -moz-linear-gradient(top, #555, #444, #444, #555);
	background-image:     -ms-linear-gradient(top, #555, #444, #444, #555);
	background-image:      -o-linear-gradient(top, #555, #444, #444, #555);
	border: 1px solid #111;
	border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
  */
	display: block;
/*	position: absolute;   */
	top: 48px; right: -10px;
}

#countdown #tiles{
	position: relative;
	z-index: 1;
}

#countdown #tiles > span{
	width: 92px;
	max-width: 92px;
	font: bold 48px 'Droid Sans', Arial, sans-serif;
	text-align: center;
  color: white;
 
	background-color: #333333;

/*	border-top: 1px solid #fff;*/
	border-radius: 3px;
	box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.7);
	margin: 0 7px;
	padding: 18px 0;
  display: inline-block;
  padding-bottom: 25px;
  font-family: lato;
	position: relative;
}

#countdown #tiles > span:before{
	content:"";
  width: 100%;
  
	height: 13px;
/*	background: #111; */
  display: block;

	padding: 0 3px;
/*	position: absolute;   */
	top: 41%; left: -3px;
	z-index: -1;
}

#countdown #tiles > span:after{
	content:"";
	width: 100%;
  height: 1px;
  
	background: #eee;
	border-top: 1px solid #333;
	display: block;
/*	position: absolute;   */
	top: 48%; left: 0;
}

#countdown .labels{
	width: 100%;
	height: 25px;
	text-align: center;
/*	position: absolute;   */
	bottom: 8px;
}

#countdown .labels li{
	width: 102px;
	font: bold 15px 'Droid Sans', Arial, sans-serif;
	color: #f47321;
  /* text-shadow: 1px 1px 0px #000;*/
  font-family: lato;
	text-align: center;
	text-transform: uppercase;
	display: inline-block;
}
  </style>

<!-- END new counter-->







<!-- question reponse 2  -->
   
<!-- END question reponse  -->
    <section id="hackNoel" class="cta">
      <div class="cta-content">
        <div class="container">

          <p style="color:white !important; font-family:lato;font-size: 19px;padding-left: 10px">  Le Module  </p>
                    <p style="color:white !important;font-family:lato; font-size: 70px; margin-top: -0.5em;" >   #Hack NoëL ! </p>

<p style="color:white !important; font-size: 30px; font-family:lato; margin-top: -0.8em; padding-left: 10px;" > NoëL arrive : <span style="font-size: 20px;">Ne rate pas l'occasion !</span>  </p>


<!--
          <h2 style="font-family: lato;"> <span style="font-family: lato; font-size:14px;">Le Module</span>
          </br>
            <span style="font-size: 70px; font-family: lato;">#Hack NoëL !</span><br><span style="font-size: 30px;"> NoëL arrive :</span> <span style="font-size: 14px; font-family: lato"> Ne rate pas l'occasion ..! </span></h2> -->
          <a href="#moduleNoel" class="btn btn-outline btn-xl js-scroll-trigger" style="color:white !important;" > Découvrir le module</a> 
        </div>
      </div>
      <div class="overlay"></div>
    </section>

  <section class="features" id="moduleNoel" style="margin-bottom: -50px;">
         <div class="container">
    <div class="col-lg-12"  >
                  <div class="feature-item questionQuatre" style="margin-left:22%; margin-right:auto; width:50%;">
                   
                    <div  style="margin-left: auto; margin-right: auto;">
                      
                       <img class="text-primary"  style="    margin-left: 80%;width: 70px;" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/ascendant-bars-graphic-3.png">
                    </div>
                   
                    <h3 style="width: 180%; font-family: lato; padding-top: 20px;" >Le module #Hack Noël : </h3>
                    <p class="text-muted" style="font-family: lato; width: 180%;"> Noël représente environ <b> 60% des revenus annuel des e-commerce !</b> Si tu n'as pas encore de boutique ou si tu n'as encore rien préparé pour l'occasion tout est encore possible !! </p>
                    <p class="text-muted" style="font-family: lato; width: 180%;">On verra dans ce module les hacks à mettre en place pour multiplier tes revenus durant cette fête. 
                    </br> Je te montrerai ce que les grands business mettent en place chaque année et comment toi aussi tu vas pouvoir t'inspirer d'eux pour avoir de vrais résultats.
                  </br>
                  <span style=""> Si tu n'as pas encore de boutique tu vas tout de même pouvoir à l'issue de ces 9 jours avoir tout à fait le temps de mettre en place une boutique optimale!</span>
                    </p>
                         <a href="#price2" class="btn btn-outline btn-xl js-scroll-trigger"  style="color: black;border:1px black solid; margin-left: 60%;"> Let's Start !</a> 
                   
                  </div>
                </div>
              </div>


  </section>



    <footer>
      <div id="footer" class="container">
        <img class="photo" style="width:10%; padding-top: 10px;" src="https://fairedudropshipping.com/wp-content/uploads/2018/12/photo.png">
        <p style="color: #eaeaea !important;font-family: lato;padding-top: 15px; font-size:14px;">Je reste disponible à <a style="color:white !important;font-family: lato;padding-top: 15px; font-size:14px;" href="mailto:mail@fairedudropshipping.com"  >mail@fairedudropshipping.com</a></p>
        <p style="color: #eaeaea !important;font-family: lato; padding-top: 20px; font-size:10px;">&copy;2018 FaireDuDropshipping.com</p>
       
        </ul>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/new-age.min.js"></script>
      <script src="js/new-age.js"></script>
        <script src="js/jquery.js"></script>
    <script src="js/flipclock.min.js"></script>
<style>     p,h2,h4,h3,h5,a,li, i {color:black !important;}  
a.nav-link.js-scroll-trigger {
    font-family: lato !important;
    font-size: 9.5px !important;
    font-weight: 400 !important;
}
a.navbar-brand.js-scroll-trigger {
    font-size: 16px;
    top: 0.6em;
    position: relative;
    margin-top: 10p;
}
#mainNav .navbar-brand {
    color: #fdcc52;
    font-family: Catamaran,Helvetica,Arial,sans-serif;
    font-weight: 200;
    letter-spacing: 1px;
}
li {
    font-size: 15px;
}
h4#price2 {
    padding-top: 5px;
    padding-bottom: 5px;
}
.card-title {
    margin-bottom: .75rem;
    font-size: 33px;
}

button.btn.btn-lg.btn-block.btn-primary {
    padding-top: 10px;
    padding-bottom: 10px;
    margin-bottom: 14px;
}

button.btn.btn-lg.btn-block.btn-outline-primary {
    padding-top: 10px;
    padding-bottom: 10px;
    margin-bottom: 14px;
}
h4.my-0.font-weight-normal {
    font-size: 23px;
    padding-top: 6px;
 

}

div#tiles {
    margin-top: -130px !important;
}
</style>
  </body>

</html>



